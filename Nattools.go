package nattools

import (
	"encoding/json"
	"log"
	"sync"
	"time"

	nats "github.com/nats-io/nats.go"
)

const _DebugEnabled = false

func toJSON(in map[string]interface{}) (out string) {
	jsonString, err := json.Marshal(in)
	if err != nil {
		log.Fatal(err)
	}

	out = string(jsonString[:len(jsonString)])
	return out
}

func fromJSON(in []byte) (out map[string]interface{}) {
	out = make(map[string]interface{})
	_ = json.Unmarshal(in, &out)
	return out
}

//Transmit sends request over NATS and recieves response in
//useful and easy-to-use Dictionary format
func Transmit(nc *nats.Conn, agentID string, request string, payload map[string]interface{}) (response map[string]interface{}) {
	payload["__ABSTRACT_SERVICE_action"] = request
	payload["__ABSTRACT_SERVICE_agentId"] = agentID

	transmit := toJSON(payload)

	if _DebugEnabled {
		log.Println("Debug> NATSActions.go> Transmiting", request, " with payload = ", toJSON(payload))
	}

	wg := sync.WaitGroup{}
	wg.Add(1)

	msg, err := nc.Request(request, []byte(transmit), 3*time.Second)

	if err != nil {
		log.Println("Debug> NATSActions.go> ERROR! Transmit completed with error = ", err)
		wg.Done()
		return make(map[string]interface{})
	}

	if msg != nil {
		result := fromJSON(msg.Data)

		if _DebugEnabled {
			log.Println("Debug> NATSActions.go> Transmit ", request, "ended with result = ", result)
		}

		wg.Done()
		return result
	}

	wg.Wait()
	return make(map[string]interface{})
}
